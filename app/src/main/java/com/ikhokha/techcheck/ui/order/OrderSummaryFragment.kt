package com.ikhokha.techcheck.ui.order


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.ikhokha.techcheck.R
import com.ikhokha.techcheck.R.layout
import com.ikhokha.techcheck.data.CartModel
import com.ikhokha.techcheck.ui.cart.CartViewModel
import com.itextpdf.text.Document
import com.itextpdf.text.DocumentException
import com.itextpdf.text.Paragraph
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_order_summary.*
import kotlinx.android.synthetic.main.fragment_order_summary.view.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class OrderSummaryFragment : Fragment() {
    lateinit var viewModel: CartViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        setHasOptionsMenu(true);

        var view = inflater.inflate(layout.fragment_order_summary, container, false)
        viewModel = ViewModelProviders.of(this).get(CartViewModel::class.java)
        val cartAdapter = SummaryAdapter(mutableListOf())
        view.summary_list?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = cartAdapter
        }
        viewModel.cartLiveData.observe(this, object : Observer<MutableList<CartModel>> {
            override fun onChanged(t: MutableList<CartModel>?) {
                Log.d("CArt items : ", t?.size.toString())
                t?.let {
                    cartAdapter.refreshItemsData(it)

                    it.forEach { item ->
                        total_price.text =
                            (total_price.text.toString().toFloat() + (item.price.toFloat() * item.quantity.toFloat())).toString()
                    }

                }
                if (t?.size?.equals(0)!!) {
                    view.no_content.visibility = View.VISIBLE
                } else {
                    view.no_content.visibility = View.GONE
                }
            }

        })
        viewModel.getCartItems()


        if (ContextCompat.checkSelfPermission(
                this.requireActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                this.requireActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this.requireActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this.requireActivity(),
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), 0
                )

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }



        return view
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {

        val item = menu?.findItem(R.id.action_summary)
        item?.setVisible(false)
    }



    override

    fun onOptionsItemSelected(item: MenuItem?): Boolean =

        when (item?.itemId) {
            R.id.action_share -> {
                createPdf(
                    File(
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath,
                        "fruits_summary.pdf"
                    )
                )
                true
            }
            else -> {
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                super.onOptionsItemSelected(item)
            }
        }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        process_order.setOnClickListener {
            viewModel.removeCartItems()
            Navigation.findNavController(it).popBackStack()
        }
        (activity as? AppCompatActivity)?.supportActionBar?.title = getString(R.string.summary_tittle)

    }


    @Throws(IOException::class, DocumentException::class)
    fun createPdf(dest: File) {
        Log.d(javaClass.simpleName, dest.absolutePath)
        val document = Document()
        PdfWriter.getInstance(document, FileOutputStream(dest))
        document.open()
        var itemCount: Int = viewModel.cartLiveData.value?.size!!
        if (itemCount == null) {
            itemCount = 0;
        }
        document.add(Paragraph("Cart Summary"));
        // document.addTitle("Cart Summary")
        val table = PdfPTable(4)
        table.addCell("Product");
        table.addCell("Quantity");
        table.addCell("Unit Price");
        table.addCell("Sub total");
        table.setHeaderRows(1)


        viewModel.cartLiveData.value!!.forEach {
            table.addCell(it.description)
            table.addCell(it.quantity.toString())
            table.addCell(it.price.toString())
            table.addCell(getSubtotal(it).toString())

        }

        document.add(table)
        document.add(Paragraph("Total is :" + total_price.text));



        document.close()
        val pdf = dest
        if (pdf.exists()) {
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            val pdfUri = context?.let { FileProvider.getUriForFile(it, "com.ikhokha.techcheck.provider", pdf) };
            intent.putExtra(Intent.EXTRA_STREAM, pdfUri)
            intent.type = "application/pdf"
            startActivity(intent)
        } else {
            Log.i("DEBUG", "File doesn't exist")
        }
    }

    fun getSubtotal(item: CartModel): Float {
        return (item.quantity * item.price)
    }


}
