package com.ikhokha.techcheck.ui.cart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.ikhokha.techcheck.R
import com.ikhokha.techcheck.data.CartModel
import kotlinx.android.synthetic.main.cart_item.view.product_desc
import kotlinx.android.synthetic.main.cart_item.view.quantity
import kotlinx.android.synthetic.main.cart_item_detail.view.*

class CartAdapter(private var items: MutableList<CartModel>, private var itemClick: ItemClick) :
    RecyclerView.Adapter<CartAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cart_item, parent, false)

        return ViewHolder(itemView = view)
    }

    fun refreshItemsData(newItems: MutableList<CartModel>) {
        this.items = newItems
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return items.count();
    }

    interface ItemClick {
        fun setClick(cartModel: CartModel, isUpdate: Boolean)

    }

    override fun onBindViewHolder(holder: CartAdapter.ViewHolder, position: Int) {
        holder.itemView.product_desc.text = items[position].description
        //  holder.itemView.price.text=items[position].price.toString()
        holder.itemView.quantity.text = items[position].quantity.toString()
        holder.itemView.setOnClickListener {
            //   itemClick.setClick(items[position])
            val mDialogView = LayoutInflater.from(it.context).inflate(R.layout.cart_item_detail, null)
            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(it.context)
                .setView(mDialogView)
                .setTitle("Fruit Details")
            //show dialog
            val mAlertDialog = mBuilder.show()
            //login button click of custom layout
            mDialogView.price.text = items[position].price.toString()
            //  mDialogView.quantity
            mDialogView.quantity.text = items[position].quantity.toString()
            mDialogView.product_update.setOnClickListener {
                var updateModel = items[position]
                updateModel.quantity = mDialogView.quantity.text.toString().toInt()
                itemClick.setClick(updateModel, true)
                mAlertDialog.dismiss()
                //get text from EditTexts of custom layout


            }
            //cancel button click of custom layout
            mDialogView.product_remove.setOnClickListener {
                itemClick.setClick(items[position], false)
                mAlertDialog.dismiss()
            }
        }


    }
}