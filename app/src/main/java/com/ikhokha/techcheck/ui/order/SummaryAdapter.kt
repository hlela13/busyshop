package com.ikhokha.techcheck.ui.order

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ikhokha.techcheck.R
import com.ikhokha.techcheck.data.CartModel
import kotlinx.android.synthetic.main.cart_item.view.product_desc
import kotlinx.android.synthetic.main.cart_item.view.quantity
import kotlinx.android.synthetic.main.cart_item_detail.view.*
import kotlinx.android.synthetic.main.cart_item_detail.view.price
import kotlinx.android.synthetic.main.summary_item.view.*

class SummaryAdapter(private var items: MutableList<CartModel>) : RecyclerView.Adapter<SummaryAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SummaryAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.summary_item, parent, false)

        return ViewHolder(itemView = view)
    }

    fun refreshItemsData(newItems: MutableList<CartModel>) {
        this.items = newItems
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return items.count();
    }

    interface ItemClick {
        fun setClick(cartModel: CartModel)

    }

    override fun onBindViewHolder(holder: SummaryAdapter.ViewHolder, position: Int) {
        holder.itemView.product_desc.text = items[position].description
        holder.itemView.quantity.text = items[position].quantity.toString()
        holder.itemView.price.text = items[position].price.toString()
        holder.itemView.sub_total.text = (items[position].quantity * items[position].price).toString()


    }
}