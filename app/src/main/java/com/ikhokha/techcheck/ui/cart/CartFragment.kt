package com.ikhokha.techcheck.ui.cart


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.ikhokha.techcheck.R
import com.ikhokha.techcheck.data.CartModel
import com.ikhokha.techcheck.data.database.entity.ProductsEntity
import com.ikhokha.techcheck.ui.SCANNED_BARCODE
import com.ikhokha.techcheck.ui.ScanActivity
import kotlinx.android.synthetic.main.fragment_cart.view.*

class CartFragment : Fragment(), CartAdapter.ItemClick {
    override fun setClick(cartModel: CartModel, isUpdate: Boolean) {
        if (isUpdate) {
            viewModel.updateQuantity(cartModel.product_id, cartModel.quantity)
        } else {
            viewModel.removeOneCartItem(cartModel.product_id)
        }
    }

    lateinit var viewModel: CartViewModel
    val BARCODE_SCANNER_REQUEST = 1
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view = inflater.inflate(R.layout.fragment_cart, container, false)
        setHasOptionsMenu(true);
        viewModel = ViewModelProviders.of(this).get(CartViewModel::class.java)
//        (requireActivity()).setActionBar(toolbar)

        var loggedIn = viewModel.login()
        if (!loggedIn) {
            Toast.makeText(context, "Failed to login please check credentials", Toast.LENGTH_LONG).show()
        }
        if (ContextCompat.checkSelfPermission(
                this.requireActivity(),
                Manifest.permission.READ_CONTACTS
            )
            != PackageManager.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this.requireActivity(),
                    Manifest.permission.READ_CONTACTS
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this.requireActivity(),
                    arrayOf(Manifest.permission.CAMERA), 0
                )

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
        view.add_product.setOnClickListener {
            val intent = Intent(context, ScanActivity::class.java)
            startActivityForResult(intent, BARCODE_SCANNER_REQUEST)

        }
        val cartAdapter = CartAdapter(mutableListOf(), this)
        view.cart_list?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = cartAdapter
        }
        viewModel.productsLiveData.observe(this, object : Observer<MutableList<ProductsEntity>> {
            override fun onChanged(t: MutableList<ProductsEntity>?) {
                //  t?.let { cartAdapter.refreshItemsData(it) }
            }

        })
        viewModel.cartLiveData.observe(this, object : Observer<MutableList<CartModel>> {
            override fun onChanged(t: MutableList<CartModel>?) {
                Log.d("CArt items : ", t?.size.toString())
                t?.let {
                    cartAdapter.refreshItemsData(it)

                }
                if (t?.size?.equals(0)!!) {
                    view.no_cart.visibility = View.VISIBLE
                } else {
                    view.no_cart.visibility = View.GONE
                }
            }

        })
        viewModel.getCartItems()
        return view
    }


    override fun onPrepareOptionsMenu(menu: Menu?) {

        val item = menu?.findItem(R.id.action_share)
        item?.setVisible(false)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
        // return super.onOptionsItemSelected(item)

        when (item?.itemId) {
            R.id.action_share -> {
                // User chose the "Settings" item, show the app settings UI...
                true
            }

            R.id.action_summary -> {
                view?.let { Navigation.findNavController(it).navigate(R.id.action_cart_to_orderSummaryFragment) }
                true
            }

            else -> {
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                super.onOptionsItemSelected(item)
            }
        }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val product_id = data?.getStringExtra(SCANNED_BARCODE)
//        Toast.makeText(context, "Scanned barcode : " + product_id, Toast.LENGTH_LONG).show()
        Log.d("Scanning results", "Done")
        if (requestCode == BARCODE_SCANNER_REQUEST) {
            product_id?.let {
                viewModel.productExists(it) { isExist ->
                    if (isExist) {
                        viewModel.addToCart(product_id, 1)
                    } else {
                        Log.d("Scanning results", "Product does not exist.")
//                        Toast.makeText(context, "Scanned barcode : " + product_id +"  does not exist.", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title =getString(R.string.cart_tittle)
    }

}
