package com.ikhokha.techcheck.ui

import android.os.Bundle
import android.app.Activity
import android.util.Log
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView
import android.content.Intent

const val SCANNED_BARCODE = "scanned_product_barcode"

class ScanActivity : Activity(), ZXingScannerView.ResultHandler {
    private var mScannerView: ZXingScannerView? = null
    val TAG = "ScanACTIVITY"

    public override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        mScannerView = ZXingScannerView(this)   // Programmatically initialize the scanner view
        setContentView(mScannerView)
    }

    public override fun onResume() {
        super.onResume()
        mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView!!.startCamera()
    }

    public override fun onPause() {
        super.onPause()
        mScannerView!!.stopCamera()
    }


    override fun handleResult(rawResult: Result) {

        val resultIntent = Intent()
        resultIntent.putExtra(SCANNED_BARCODE, rawResult.getText())
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
        mScannerView!!.resumeCameraPreview(this)
    }
}
