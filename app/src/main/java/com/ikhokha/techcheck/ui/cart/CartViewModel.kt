package com.ikhokha.techcheck.ui.cart

import androidx.lifecycle.MutableLiveData
import com.ikhokha.techcheck.BaseViewModel
import com.ikhokha.techcheck.data.CartModel
import com.ikhokha.techcheck.data.database.entity.ProductsEntity
import com.ikhokha.techcheck.data.repo.CartRepo
import com.ikhokha.techcheck.data.repo.ProductsRepo

class CartViewModel : BaseViewModel() {
    var productsRepo: ProductsRepo = ProductsRepo()
    var cartRepo: CartRepo = CartRepo()
    val productsLiveData: MutableLiveData<MutableList<ProductsEntity>> by lazy {
        MutableLiveData<MutableList<ProductsEntity>>()
    }
    val cartLiveData: MutableLiveData<MutableList<CartModel>> by lazy {
        MutableLiveData<MutableList<CartModel>>()
    }

    fun login(): Boolean {
        val loggedIn = productsRepo.loginToFirebase()
        if (loggedIn) {
            getProducts()
            return true
        } else {
            return false
        }
    }

    fun getProducts() {
        productsRepo.updateProductLiveData {
            productsLiveData.postValue(it)
        }
    }

    fun getCartItems() {

        cartRepo.updateCartLiveData {
            cartLiveData.postValue(it)
        }
    }

    fun addToCart(product_id: String, quantiy: Int) {
        cartRepo.addToCart(product_id, quantiy)
        getCartItems()
    }

    fun productExists(product_id: String, predicate: (Boolean) -> Unit) {
        productsRepo.getProduct(product_id) {
            predicate(it.size > 0)
        }
    }

    fun removeCartItems() {
        cartRepo.removeAllCartItems {
            getCartItems()
        }

    }

    fun removeOneCartItem(productId: String) {
        cartRepo.removeCartItem(productId) {
            getCartItems()
        }

    }

    fun updateQuantity(productId: String, quantiy: Int) {
        cartRepo.updateQuantity(productId, quantiy) {
            getCartItems()
        }

    }

}