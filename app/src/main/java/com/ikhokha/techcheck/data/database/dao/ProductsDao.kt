package com.ikhokha.techcheck.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ikhokha.techcheck.data.database.entity.PRODUCTS_TABLE
import com.ikhokha.techcheck.data.database.entity.ProductsEntity
import io.reactivex.Flowable

@Dao
interface  ProductsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg productsEntity: ProductsEntity)


    @Query("select * from $PRODUCTS_TABLE")
    fun getAll(): Flowable<MutableList<ProductsEntity>>

    @Query("select * from $PRODUCTS_TABLE where product_id=:productId")
    fun getProduct(productId : String): Flowable<MutableList<ProductsEntity>>




}