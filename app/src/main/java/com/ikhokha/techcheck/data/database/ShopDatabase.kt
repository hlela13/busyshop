package com.ikhokha.techcheck.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ikhokha.techcheck.data.database.dao.CartDao
import com.ikhokha.techcheck.data.database.dao.ProductsDao
import com.ikhokha.techcheck.data.database.entity.CartEntity
import com.ikhokha.techcheck.data.database.entity.ProductsEntity


const val DATABASE_VERSION = 1
const val DATABASE_NAME = "shop_db"

@Database(
    entities = [
        ProductsEntity::class,
        CartEntity::class
    ],
    version = DATABASE_VERSION,
    exportSchema = false
)
abstract class ShopDatabase : RoomDatabase() {
    abstract fun provideProduct(): ProductsDao
    abstract fun provideCart(): CartDao

    companion object {

        @Volatile
        private var instance: ShopDatabase? = null

        //    @Inject
        fun getInstance(context: Context): ShopDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): ShopDatabase {
            return Room.databaseBuilder(context, ShopDatabase::class.java, DATABASE_NAME)
                .allowMainThreadQueries()
                .build()
        }
    }
}