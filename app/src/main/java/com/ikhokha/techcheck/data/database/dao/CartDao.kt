package com.ikhokha.techcheck.data.database.dao

import androidx.room.*
import com.ikhokha.techcheck.data.CartModel
import com.ikhokha.techcheck.data.database.entity.CART_TABLE
import com.ikhokha.techcheck.data.database.entity.CartEntity
import com.ikhokha.techcheck.data.database.entity.PRODUCTS_TABLE
import io.reactivex.Flowable
@Dao
interface  CartDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg productsEntity: CartEntity)


    @Query("select * from $CART_TABLE")
    fun getAll(): Flowable<MutableList<CartEntity>>

    @Query("select * from $CART_TABLE WHERE product_id = :productId")
    fun getCartItem(productId: String): MutableList<CartEntity>

    @Query("UPDATE $CART_TABLE SET quantity = quantity + 1 WHERE product_id = :productId")
    fun updateCartItem(productId: String)

    @Query("UPDATE $CART_TABLE SET quantity = :quantity WHERE product_id = :productId")
    fun updateCartQuantity(productId: String, quantity: Int)

    @Query("select p.*,c.quantity from  $CART_TABLE c JOIN $PRODUCTS_TABLE "+" p ON c.product_id=p.product_id")
    fun getCartItems(): Flowable<MutableList<CartModel>>

    @Query("DELETE FROM $CART_TABLE")
    fun removeAll()
    @Query("DELETE FROM $CART_TABLE WHERE product_id = :productId")
    fun removeOne(productId: String)

}