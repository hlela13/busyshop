package com.ikhokha.techcheck.data.database.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import io.reactivex.annotations.NonNull

const val PRODUCTS_TABLE="products"
@Entity(tableName = PRODUCTS_TABLE,indices = [Index("product_id",unique = true)] )
 data class ProductsEntity(
    @PrimaryKey
    @NonNull

    var product_id: String,
    var price : Float?,
    var description : String?



)
