package com.ikhokha.techcheck.data

import androidx.room.PrimaryKey
import io.reactivex.annotations.NonNull

class CartModel {
    var product_id: String
    var price : Float
    var description : String?
    var quantity : Int

    constructor(product_id: String, price: Float, description: String?, quantity: Int) {
        this.product_id = product_id
        this.price = price
        this.description = description
        this.quantity = quantity
    }
}
