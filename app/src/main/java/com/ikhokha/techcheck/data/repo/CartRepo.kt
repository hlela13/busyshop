package com.ikhokha.techcheck.data.repo

import com.ikhokha.techcheck.BaseApplication
import com.ikhokha.techcheck.data.CartModel
import com.ikhokha.techcheck.data.database.entity.CartEntity
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class CartRepo {
    val localdb = BaseApplication.getDatabase()

    fun updateCartLiveData(callback: (MutableList<CartModel>) -> Unit): Disposable {
        return localdb!!.provideCart().getCartItems().subscribeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    callback(result)
                },

                Throwable::printStackTrace
            )

    }

    fun addToCart(product_id: String, quantiy: Int) {


        var cartEntity = CartEntity(product_id, quantity = quantiy)

        insertOrUpdate(cartEntity)

    }


    fun insertOrUpdate(cartEntity: CartEntity) {
        if (localdb != null) {
            var cartItems = localdb.provideCart().getCartItem(cartEntity.product_id)
            if (cartItems.isEmpty()) {
                localdb.provideCart().insertAll(cartEntity)

            } else {
                localdb.provideCart().updateCartItem(cartEntity.product_id)
            }

        }
    }

    fun removeAllCartItems(callback: (Boolean) -> Unit): Disposable {
        return Single.fromCallable {
            localdb?.provideCart()?.removeAll()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result -> callback(true) })


    }

    fun removeCartItem(product_id: String, callback: (Boolean) -> Unit): Disposable {
        return Single.fromCallable {
            localdb?.provideCart()?.removeOne(product_id)
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result -> callback(true) })


    }

    fun updateQuantity(product_id: String, quantity: Int, callback: (Boolean) -> Unit): Disposable {
        return Single.fromCallable {
            localdb?.provideCart()?.updateCartQuantity(product_id, quantity)
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result -> callback(true) })


    }
}