package com.ikhokha.techcheck.data.repo

import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.ikhokha.techcheck.BaseApplication
import com.ikhokha.techcheck.data.database.entity.ProductsEntity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ProductsRepo {


    val localdb = BaseApplication.getDatabase()
    private lateinit var auth: FirebaseAuth

    private lateinit var database: DatabaseReference
    val TAG = "REPO"

    fun updateProductLiveData(callback: (MutableList<ProductsEntity>) -> Unit): Disposable {
        return localdb!!.provideProduct().getAll().subscribeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    callback(result)
                },

                Throwable::printStackTrace
            )

    }

    fun loginToFirebase(): Boolean {
        var loggedIn: Boolean = false
        auth = FirebaseAuth.getInstance()
        if (auth.currentUser == null) {
            var that = this
            auth.signInWithEmailAndPassword("techcheck@ikhokha.com", "password").addOnCompleteListener() {
                if (it.isSuccessful) {
                    Log.d(TAG, "signInWithEmail:success")
                    Log.d(TAG, "Passed  login")
                    database = FirebaseDatabase.getInstance().reference
                    retrieveProducts()
                    //  database.child()
                    loggedIn = true
                } else {
                    Log.d(TAG, "Failed to login")
                    loggedIn = false
                }
            }
            false
        } else {

            retrieveProducts()
            var tl = database.child("data")
            // var jd =tl.database

            return true;

        }
        return loggedIn
    }

    fun retrieveProducts() {
        val postListener = object : ValueEventListener, ChildEventListener {
            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                Log.d(TAG, "moved")
            }

            override fun onChildAdded(data: DataSnapshot, p1: String?) {
                var product = data.getValue()
                //var desc=""
//                if(data.hasChild("description")){
                val desc: HashMap<*, *> = data.getValue(true) as HashMap<*, *>

                var image = ""
                var price = 0f
                var description = "";


                desc.forEach {
                    Log.d("NEW_DATA", "${it.key.toString()} = ${it.value.toString()}")

                    when (it.key.toString()) {
                        "image" -> image = it.value.toString()
                        "price" -> price = it.value.toString().toFloat()
                        "description" -> description = it.value.toString()
                    }


                }
                var productId: String = data.key.toString()
                var productsEntity = ProductsEntity(product_id = productId, price = price, description = description);
                //  var productsEntityList = MutableList<ProductsEntity>()
                //  productsEntityList.add(productsEntity)
                if (localdb != null) {
                    localdb.provideProduct().insertAll(productsEntity)
                }

                Log.d(TAG, "moved")
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                Log.d(TAG, "moved")
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                Log.d(TAG, "moved")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                val post = dataSnapshot.getValue()
                // ...
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException())

            }
        }
        database = FirebaseDatabase.getInstance().reference
        database.addChildEventListener(postListener)
    }


    fun getProduct(product_id: String, callback: (MutableList<ProductsEntity>) -> Unit): Disposable {
        return localdb!!.provideProduct().getProduct(product_id).subscribeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    callback(result)
                },

                Throwable::printStackTrace
            )

    }


}