package com.ikhokha.techcheck.data.database.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import io.reactivex.annotations.NonNull

const val CART_TABLE="cart"
@Entity(tableName = CART_TABLE,indices = [Index("product_id",unique = true)] )
 data class CartEntity(
    @PrimaryKey
    @NonNull
    var product_id: String,
    var quantity : Int



)
