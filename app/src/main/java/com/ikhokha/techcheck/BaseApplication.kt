package com.ikhokha.techcheck

import android.app.Application
import com.facebook.stetho.Stetho
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.ikhokha.techcheck.data.database.ShopDatabase
import com.ikhokha.techcheck.data.database.ShopDatabase.Companion.getInstance


class BaseApplication : Application() {

    public lateinit var appInstance: BaseApplication

    companion object {
        var db: ShopDatabase? = null
        fun getDatabase(): ShopDatabase? {
            return db
        }

    }

    override fun onCreate() {
        super.onCreate()
        appInstance = this;
        Stetho.initializeWithDefaults(this)
        db = getInstance(applicationContext)
        val options = FirebaseOptions.Builder()
            .setApplicationId("1:1094480502840:android:d78786cba5811378")
            .setApiKey("AIzaSyDH8lDCbVv8ktkdhHamOty-UCU9tzOt5RA")
            .setDatabaseUrl("https://the-busy-shop.firebaseio.com/")
            .setStorageBucket("the-busy-shop.appspot.com").build()
        FirebaseApp.initializeApp(this, options)
    }
}